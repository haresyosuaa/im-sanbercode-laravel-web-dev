<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Second Page</title>
</head>
<body>
  <form action="/register" method="post">
    @csrf

  <h1>Buat Account Baru!</h1>
  <h3>Sign Up Form</h3>

  <label>First name:</label><br>
  <input type="text" name="first_name"><br><br>

  <label>Last name:</label><br>
  <input type="text" name="last_name"><br><br>

  <label>Gender:</label><br>
  <input type="radio" name="gender">Male<br>
  <input type="radio" name="gender">Female<br>
  <input type="radio" name="gender">Other<br><br>

  <label>Nationality:</label><br><br>
  <select name="nationality">
    <option value="0">Indonesian</option>
    <option value="1">Singaporean</option>
    <option value="2">Malaysian</option>
    <option value="3">Australian</option>
  </select><br><br>

  <label>Language Spoken:</label> <br>
  <input type="checkbox" name="languange">Bahasa Indonesia <br>
  <input type="checkbox" name="languange">English <br>
  <input type="checkbox" name="languange">Other <br><br>

  <label>Bio:</label> <br>
  <textarea name="bio" cols="30" rows="10"></textarea><br><br>

  <input type="submit" name="form" value="Sign Up">
</form>
</body>
</html>