<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CastController;
use Illumintae\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('home');
// });

// Route::get('/master', function(){
//     return view('layout.master');
// });

// Route::get('/table', function () {
//     return view('layout.table');
// });

// Route::get('/data-table', function () {
//     return view('layout.datatable');
// });

//Create Data
//ke form tambah
Route::get('/cast/create', [CastController::class, 'create']);

//Route tambah data ke database
Route::post('/cast', [CastController::class, 'store']);

//Read Data
//untuk tampil semua cast
Route::get('/cast', [CastController::class, 'index']);
//untuk detail cast berdasarkan id
Route::get('/cast/{id}', [CastController::class, 'show']);

//updateData
//route untuk mengarah ke form edit data dengan params id;
Route::get('/cast/{id}/edit', [CastController::class, 'edit']);
//route update data di database
Route::put('/cast/{id}', [CastController::class, 'update']);

//delete data;
Route::delete('cast/{id}', [CastController::class, 'destroy']);