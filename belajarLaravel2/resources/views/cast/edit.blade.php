@extends('layout.master')
@section('title')
Halaman Edit Cast
@endsection
@section('content')

<form action="/cast/{{$cast->id}}" method="POST">
  @csrf
  @method('put')
  <div class="form-group">
    <label>Cast Name</label>
    <input type="text" value="{{$cast->nama}}" name="nama" class="@error('nama') is-invalid @enderror form-control" placeholder="Enter Cast Name">
  </div>
  @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror

  <div class="form-group">
    <label>Cast Age</label>
    <input type="number" value="{{$cast->umur}}" name="umur" class="@error('umur') is-invalid @enderror form-control" placeholder="Enter Cast Age">
  </div>
  @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror

  <div class="form-group">
    <label>Cast Biography</label>
    <textarea class="@error('bio') is-invalid @enderror form-control" name="bio"  cols="30" rows="10" placeholder="Enter Biography">{{$cast->bio}}</textarea>
  </div>
  @error('bio')
  <div class="alert alert-danger">{{ $message }}</div>
  @enderror

  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection
