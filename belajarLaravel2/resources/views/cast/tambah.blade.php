@extends('layout.master')
@section('title')
Halaman Tambah Cast
@endsection
@section('content')

<form action="/cast" method="POST">
  @csrf
  <div class="form-group">
    <label>Cast Name</label>
    <input type="text" name="nama" class="@error('nama') is-invalid @enderror form-control" placeholder="Enter Cast Name">
  </div>
  @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror

  <div class="form-group">
    <label>Cast Age</label>
    <input type="number" name="umur" class="@error('umur') is-invalid @enderror form-control" placeholder="Enter Cast Age">
  </div>
  @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror

  <div class="form-group">
    <label>Cast Biography</label>
    <textarea class="@error('bio') is-invalid @enderror form-control" name="bio"  cols="30" rows="10" placeholder="Enter Biography"></textarea>
  </div>
  @error('bio')
  <div class="alert alert-danger">{{ $message }}</div>
  @enderror

  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection
